import java.util.Scanner;
//Entwickeln Sie ein Programm, welches ausgibt, ob das eingegebene Jahr ein Schaltjahr ist.
//Regel 1: Ist eine Jahreszahl ganzzahlig durch 4 teilbar, dann ist das Jahr ein Schaltjahr mit
//366 Tagen.
//Beispiele: 1980, 1972, 1720 waren Schaltjahre
//Regel 2: Ausnahme von Regel 1 sind alle Jahreszahlen, die nach Regel 1 ein Schaltjahr
//sind, aber deren Jahreszahl ganzzahlig durch 100 teilbar sind.
//Beispiele: 1700, 1800 und 1900 oder ferner 2100 sind keine Schaltjahre.
//Regel 3: Ausnahme von Regel 2 sind alle Jahreszahlen, die nach Regel 2 kein Schaltjahr
//sind, aber deren Jahreszahl ganzzahlig durch 400 teilbar.
//Beispiele: 1600 und 2000 waren Schaltjahre zu 366 Tagen.


public class Schaltjahr {

	public static void main(String[] args) {
		int ein = 0;
		boolean ee;
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Jahreszahl an:");
		ein = sc.nextInt();
		ee = rechne(ein);
		if (ee) {
			System.out.println(ein + " ist ein Schaltjahr!");
		}
		else {
			System.out.println(ein + " ist kein Schaltjahr!");
		}
		
	}

	
	
	public static boolean rechne(int x) {
		boolean e = false;
		if (x % 4 == 0) {
			e = true;
		}
		if (x%100 == 0) {
			e = false;
		}
		if (x%400 == 0) {
			e = true;
		}
		return e;
	}
}
