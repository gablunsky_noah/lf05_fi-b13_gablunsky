/**@author Noah Gablunsky
 * 3 Sterne Aufgabe
 */





import java.util.*;
public class Raumschiff
{

	/**
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
    private String schiffsname;
    private int photonentorpedos;
    private int energieversorgung;
    private int schilde;
    private int huelle;
    private int lebenserhaltungssysteme;
    private int androidenAnzahl;
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>() ;
    public static ArrayList<String> broadcastKommunikator = new ArrayList<String>() ;
    
    //Konstruktoren
    public Raumschiff() {};

    public Raumschiff( String name, int photonentorpedos, int energieversorgung, int schilde, int huelle, int lebenserhaltungssysteme, int androidenAnzahl )
    {
        this.schiffsname = name;
        this.photonentorpedos = photonentorpedos;
        this.energieversorgung = energieversorgung;
        this.schilde = schilde;
        this.huelle = huelle;
        this.lebenserhaltungssysteme = lebenserhaltungssysteme;
        this.androidenAnzahl = androidenAnzahl;
    }

    // Verwaltungsmethoden


    public int getPhotonentorpedos()
    {
        return this.photonentorpedos;
    }

    public void setPhotonentorpedos( int y )
    {
        this.photonentorpedos = y;
    }

    public String getSchiffsname()
    {
        return this.schiffsname;
    }

    public void setSchiffsname( String y )
    {
        this.schiffsname = y;
    }

    public int getEnergieversorgung()
    {
        return this.energieversorgung;
    }

    public void setEnergieversorgung( int y )
    {
        this.energieversorgung = y;
    }

    public int getSchilde()
    {
        return this.schilde;
    }

    public void setSchilde( int y )
    {
        this.schilde = y;
    }

    public int getHuelle()
    {
        return this.huelle;
    }

    public void setHuelle( int y )
    {
        this.huelle = y;
    }

    public int getLebenserhaltungssysteme()
    {
        return this.lebenserhaltungssysteme;
    }

    public void setLebenserhaltungssysteme( int y )
    {
        this.lebenserhaltungssysteme = y;
    }

    public int getAndroidenanzahl()
    {
        return this.androidenAnzahl;
    }

    public void setAndroidenanzahl( int y )
    {
        this.androidenAnzahl = y;
    }


    // Andere Methoden

    /**
     * F�gt ein Ladungsobjekt der ArrayList Ladungsverzeichnis hinzu
     * @param ladung  Ein zuvor erstelltes Ladungsobjekt
     */
	public void addLadung( Ladung ladung )
    {
        ladungsverzeichnis.add( ladung );
    }

	/**
	 * Alle Zust�nde (Attributwerte) des Raumschiffes auf der Konsole mit entsprechenden Namen ausgeben.
	 */
    public void status()
    {
    	System.out.println( "*****STATUS*****" );
        System.out.println("");
        System.out.println( "Der Name des Schiffes lautet: " + getSchiffsname() );
        System.out.println( "Es sind " + getPhotonentorpedos() + " Photonentorpedos an Bord." );
        System.out.println( "Die Energiereserven des Schiffes sind bei " + getEnergieversorgung() + "%." );
        System.out.println( "Die Schilde sind bei " + getSchilde() + "%." );
        System.out.println( "Die Hülle ist bei " + getHuelle() + "% Haltbarkeit." );
        System.out.println( "Die Lebenserhaltungssysteme sind bei " + getLebenserhaltungssysteme() + "%" );
        System.out.println( "Es sind " + getAndroidenanzahl() + " Androiden an Bord." );
        System.out.println("");
        System.out.println( "***END STATUS***");
        System.out.println("");
        }

    /**
     * Gibt es keine Ladung Photonentorpedo auf dem Schiff, wird als Nachricht Keine Photonentorpedos gefunden! in der Konsole ausgegeben und die Nachricht an alle -=*Click*=- ausgegeben.
	 * Ist die Anzahl der einzusetzenden Photonentorpedos gr��er als die Menge der tats�chlich Vorhandenen, so werden alle vorhandenen Photonentorpedos eingesetzt.
	 * Ansonsten wird die Ladungsmenge Photonentorpedo �ber die Setter Methode vermindert und die Anzahl der Photonentorpedos im Raumschiff erh�ht.
	 * Konnten Photonentorpedos eingesetzt werden, so wird die Meldung [X] Photonentorpedo(s) eingesetzt auf der Konsole ausgegeben. [X] durch die Anzahl ersetzen.
     * @param anzahl  Die gew�nschte Anzahl an verladenen Photonentorpedos
     */
    public void laden( int anzahl )
    {
    	boolean erf = false;
    	for( int i = 0; i < ladungsverzeichnis.size(); i++ )
    	{
    		if( ladungsverzeichnis.get(i).getBezeichnung().equalsIgnoreCase( "photonentorpedo" ) && ladungsverzeichnis.get(i).getMenge() != 0 )
    		{
    			if( anzahl > ladungsverzeichnis.get(i).getMenge() )
    			{
    				anzahl = ladungsverzeichnis.get(i).getMenge();
    				ladungsverzeichnis.get(i).setMenge(0);
    				setPhotonentorpedos( getPhotonentorpedos() + anzahl );
    				System.out.println( "Es wurden " + anzahl + " Photonentorpedos eingesetzt." );
    				erf = true;
    				aufr�umen();
    			}
    			else 
    			{
    				ladungsverzeichnis.get(i).setMenge( ladungsverzeichnis.get(i).getMenge() - anzahl );
    				setPhotonentorpedos( getPhotonentorpedos() + anzahl );
    				System.out.println( "Es wurden " + anzahl + " Photonentorpedos eingesetzt." );
    				erf = true;
    				aufr�umen();
    			}
    		}
    	}
		if( !erf )
		{
			System.out.println( "Es konnten keine Photonentorpedos gefunden werden." );
			Broadcast( "-=*Click*=-" );
		}
    }
    
    /**
     * Verwendet die gew�nschte Anzahl, maximal die vorhandene Anzahl an Reparaturandroiden und repariert die gew�nschten Strukturen
     * @param ss	Schutzschilde
     * @param ev	Energieversorgung
     * @param sh	Schiffsh�lle
     * @param ad	Androidenanzahl
     */
    public void reparatur( boolean ss, boolean ev, boolean sh, int ad )
    {
    	Random a = new Random();
    	int ran = a.nextInt(99) + 1;
    	//int ben = ad;
    	
    	int n = 0;
    	
    	if( ad > getAndroidenanzahl() )
    		ad = getAndroidenanzahl();
    	
    	if(ss)
    		n++;
    	if(ev)
    		n++;
    	if(sh)
    		n++;
    	
    	int rechnung = (ran * ad) / n;
    	
    	if(ss)
   			System.out.println( "Die Schilde wurden um " + ( 100 - getSchilde() ) + " Punkte repariert." );
    		setSchilde(getSchilde()+rechnung);
    		if( getSchilde() > 100 )
    			setSchilde(100);
    	if(ev)
   			System.out.println( "Die Energieversorgung wurden um " + ( 100 - getEnergieversorgung() ) + " Punkte repariert." );
    		setEnergieversorgung(getEnergieversorgung()+rechnung);
    		if( getEnergieversorgung() > 100 )
    			setEnergieversorgung(100);
    	if(sh)
   			System.out.println( "Die Huelle wurden um " + ( 100 - getHuelle() ) + " Punkte repariert." );
    		setHuelle(getHuelle()+rechnung);
    		if( getHuelle() > 100 )
    			setHuelle(100);
    	
//    	System.out.println("Die Strukturen wurden um " + rechnung + " Punkte repariert.");
    }
    
    /**
     * Alle Ladungen eines Raumschiffes auf der Konsole mit Ladungsbezeichnung und Menge ausgeben.
     */
    public void Ladeliste()
    {
       	System.out.println( "***LADUNGSVERZEICHNIS***" );
        System.out.println("");
        for( int i = 0; i < ladungsverzeichnis.size(); i++ )
        {
            System.out.print( ladungsverzeichnis.get(i).getBezeichnung() + "," );
            System.out.println( " " + ladungsverzeichnis.get(i).getMenge() );
        }
       System.out.println("");
       System.out.println( "***ENDE LADUNGSVERZEICHNIS***" );
       System.out.println("");
       System.out.println("");
    }
    
    /**
     * Durchsucht das Ladungsverzeichnis nach Objekten, deren Anzahl 0 entspricht und entfernt diese.
     */
    public void aufr�umen()
    {
    	for( int i = 0; i < ladungsverzeichnis.size(); i++ )
    	{
    		if( ladungsverzeichnis.get(i).getMenge() == 0 )
    		{
    			System.out.println( ladungsverzeichnis.get(i).getBezeichnung() + " wurde aus dem Ladungsverzeichnis gel�scht." );
    			ladungsverzeichnis.remove(i);
    			i--;
    		}
    	}
    }

    /**
     * Gibt es keine Torpedos, so wird als Nachricht an Alle -=*Click*=- ausgegeben.
	 * Ansonsten wird die Torpedoanzahl um eins reduziert und die Nachricht an Alle Photonentorpedo abgeschossen gesendet. Au�erdem wird die Methode Treffer aufgerufen
     * @param ziel Das Raumschiffobjekt, welches als Ziel gew�hlt wurde.
     */
    public void Torpedoschuss( Raumschiff ziel )
    {
        if( getPhotonentorpedos() > 0 )
        {
            setPhotonentorpedos( getPhotonentorpedos() - 1 );
            String f = "Photonentorpedo abgeschossen";
            System.out.println( f );
            broadcastKommunikator.add(f);
        	System.out.println( "Das Schiff " + this.getSchiffsname() + " schie�t mit Photonentorpedos auf das Schiff " + ziel.getSchiffsname() + "!" );
        	System.out.println("");
            Treffer(ziel);
        }
        else{
        	System.out.println("Keine Photonentorpedos gefunden!");
            Broadcast( "-=*Click*=-" );
        }
    }

    /**
     * Ist die Energieversorgung kleiner als 50%, so wird als Nachricht an Alle -=*Click*=- ausgegeben.
	 * Ansonsten wird die Energieversorgung um 50% reduziert und die Nachricht an Alle �Phaserkanone abgeschossen� gesendet. Au�erdem wird die Methode Treffer aufgerufen.
     * @param ziel Das Raumschiffobjekt, welches als Ziel gew�hlt wurde.
     */
    public void Phaserschuss( Raumschiff ziel )
    {
        if( getEnergieversorgung() >= 50 )
        {
        	System.out.println( "Das Schiff " + this.getSchiffsname() + " schie�t mit Phasern auf das Schiff " + ziel.getSchiffsname() + "!" );
        	System.out.println("");
            setEnergieversorgung( getEnergieversorgung() -50 );
            String f = "Phaserkanone abgeschossen";
            System.out.println( f );
            broadcastKommunikator.add(f);
            Treffer( ziel );
        }
        else
        {
            System.out.println( "-=*Click*=-" );
        }
    }

    /**
     * Gibt alle im BroadcastKommunikator gespeicherten Nachrichten aus.
     */
    public void auslesen()
    {
    	for( int i = 0; i < broadcastKommunikator.size(); i++ )
    	{
    		System.out.println( broadcastKommunikator.get(i) );
    	}
    }
    
    /**
     * Die Schilde des getroffenen Raumschiffs werden um 50% geschw�cht.
	 * Sollte anschlie�end die Schilde vollst�ndig zerst�rt worden sein, so wird der Zustand der H�lle und der Energieversorgung jeweils um 50% abgebaut.
	 * Sollte danach der Zustand der H�lle auf 0% absinken, so sind die Lebenserhaltungssysteme vollst�ndig zerst�rt und es wird eine Nachricht an Alle ausgegeben, dass die Lebenserhaltungssysteme vernichtet worden sind.
     * @param y
     */
    private void Treffer( Raumschiff y )
    {
        System.out.println( "Das Raumschiff " + y.getSchiffsname() + " wurde getroffen!" );
        System.out.println("");
        if( y.getSchilde() <= 50 )
        {
        	y.setSchilde(0);
        	y.setHuelle(y.getHuelle()-50);
        	y.setEnergieversorgung(y.getEnergieversorgung()-50);
        }
        if( y.getHuelle() < 1 )
        {
        	y.setLebenserhaltungssysteme(0);
        	y.Broadcast("Die Lebenserhaltungssysteme des Raumschiffs " + y + " sind vollst�ndig zerst�rt worden!");
        	System.out.println("");
        }
    }

    /**
     * Die Nachricht wird dem broadcastKommunikator hinzugef�gt und auf der Konsole ausgegeben.
     * @param Nachricht Ein String, der die gew�nschte Nachricht enth�lt.
     */
    public void Broadcast( String Nachricht )
    {
        System.out.println( "(ALLE)" + this.getSchiffsname() + ": " + Nachricht );
        broadcastKommunikator.add(Nachricht);
    }
}