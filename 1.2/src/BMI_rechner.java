import java.util.Scanner;
//(K�rpergewicht in kg)/(K�rpergr��e in m)^2
public class BMI_rechner {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double zwischenergebnis = 0;
		double gewicht = 0;
		double gr��e = 0;
		String geschlecht = "";
		System.out.println("Bitte gib dein Gewicht in kg an:");
		gewicht = sc.nextDouble();
		System.out.println("Bitte gib deine Gr��e in cm an:");
		gr��e = sc.nextDouble();
		System.out.println("Bitte gib dein Geschlecht an(m/w):");
		geschlecht = sc.next();
		zwischenergebnis = rechne(gewicht, gr��e);
		String endergebnis = ende(zwischenergebnis, geschlecht);
		System.out.println("Sie sind " + endergebnis + "!");
		
	}
	
	
	public static String ende (double c, String v) {
		String ee;
		if (v.equalsIgnoreCase("m")) {
			if (c < 20) {
				ee = "untergewichtig";
			}
			else {
				if (c >= 20 && c <= 25) {
					ee = "normalgewichtig";
				}
				else {
					if ( c > 25) {
						ee = "�bergewichtig";
					}
					else {
						ee = "";
					}
				}
			}
		}
		else {
			if (c < 19) {
				ee = "untergewichtig";
			}
			else {
				if (c >= 19 && c <= 24) {
					ee = "normalgewichtig";
				}
				else {
					if (c > 24) {
						ee = "�bergewichtig";
					}
					else {
						ee="";
					}
				}
			}
		}
		return ee;
	}
	
	
	
	public static double rechne(double x, double y) { //berechnung des BMI
		y = y/100;
		double ze = (x)/(y*y);
		return ze;
	}

}
