public class Try {
    public static void main(String[] args)
    {
        Raumschiff klingonen = new Raumschiff( "IKS Hegh'ta", 1, 100, 100, 100, 100, 2 );
        Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
        klingonen.addLadung(l1);
        klingonen.addLadung(l2);
        Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
        Ladung l3 = new Ladung("Borg-Schrott", 5);
        Ladung l4 = new Ladung("Rote Materie", 2);
        Ladung l5 = new Ladung("Plasma-Waffe", 50);
        romulaner.addLadung(l3);
        romulaner.addLadung(l4);
        romulaner.addLadung(l5);
        Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80, 80, 50, 100, 5);
        Ladung l6 = new Ladung("Forschungssonde", 35);
        Ladung l7 = new Ladung("Photonentorpedo", 3);
        vulkanier.addLadung(l6);
        vulkanier.addLadung(l7);
        klingonen.Torpedoschuss(romulaner);
        romulaner.Phaserschuss(klingonen);
        vulkanier.Broadcast("Gewalt ist nicht logisch.");
        klingonen.status();
        klingonen.Ladeliste();
        vulkanier.reparatur(true, true, true, 15);
        vulkanier.laden(2);
        vulkanier.aufr�umen();
        klingonen.Torpedoschuss(romulaner);
        klingonen.Torpedoschuss(romulaner);
        klingonen.status();
        System.out.println("");
        klingonen.Ladeliste();
        romulaner.status();
        System.out.println("");
        romulaner.Ladeliste();
        vulkanier.status();
        System.out.println("");
        vulkanier.Ladeliste();
        vulkanier.auslesen();
    }
}