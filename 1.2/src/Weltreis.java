import java.util.Scanner;
public class Weltreis {

	public static void main(String[] args) {
		Scanner ma = new Scanner(System.in);
		boolean lw = true;;
		double budg = bud();
		while (lw == true) {
			lw = false;
			while (lw == false) {
				String dess = dest();
				rechne(budg, dess);
				while (budg > 100 && lw == false) {
					budg = wechsel(budg);
					System.out.println("Dein verbleibendes Budget beträgt " + budg + " Euro!");
					System.out.println("Möchtest du das Land wechseln? (J/N)");
					String o = ma.next();
					if (o.equalsIgnoreCase("J"))
					lw = true;
				}
			}
		}
		

	}
	
	public static double wechsel(double z) {
		Scanner dc = new Scanner(System.in);
		System.out.println("Bitte gib ein, wieviel du wechseln möchtest:");
		double w = dc.nextDouble();
		double q = z - w;
		double r = z;
		if (q < 100) {
			System.out.println("Das kannst du dir nicht mehr leisten!");
		}
		else {
			r = q;
		}
		return r;
	}
	
	public static void rechne(double x, String y) {
		double neu = 0;
		if (y.equals("die USA")) {
			neu = x*1.22;
			System.out.println("Dein Reisebudget für " + y + " beträgt " + neu + " Dollar!");
		}
		else {
			if (y.equals("Schweden")) {
				neu = x*10.10;
				System.out.println("Dein Reisebudget für " + y + " beträgt " + neu + " schwedische Kronen!");
			}
			else {
				if (y.equals("England")) {
					neu = x*0.89;
					System.out.println("Dein Reisebudget für " + y + " beträgt " + neu + " Pfund!");
				}
				else {
					if (y.equals("Schweiz")) {
						neu = x*1.08;
						System.out.println("Dein Reisebudget für " + y + " beträgt " + neu + " Schweizer Franken!");
					}
					else {
						if (y.equals("Japan")) {
							neu = x*126.50;
							System.out.println("Dein Reisebudget für " + y + " beträgt " + neu + " Yen!");
						}
					}
				}
			}
		}
	}
	
	public static double bud() {
		Scanner ssc = new Scanner(System.in);
		boolean suc = false;
		double bu = 0;
		
		while (suc == false) {
			System.out.println("Bitte gib dein Reisebudget ein:");
			bu = ssc.nextInt();
			
			if (bu <= 100) {
				System.out.println("Damit kommst du nicht weit!");
			}
			else {
				System.out.println("Dein Reisebudget beträgt " + bu + " Euro!");
				suc = true;
			}
		}
		return bu;
	}
	
	
	public static String dest() {
		Scanner sc = new Scanner(System.in);
		boolean erf = false;
		String des = new String();
		double au = 0;
		
		while (erf == false) {
			System.out.println("Bitte gib dein nächstes Reiseziel ein:");
			des = sc.next();
			
			if (des.equalsIgnoreCase("USA")) {
				System.out.println("Der Wechselkurs für die USA beträgt 1 Euro > 1.22 Dollar!");
				erf = true;
				des = "die USA";
			}
			else {
				if (des.equalsIgnoreCase("Schweden")) {
					System.out.println("Der Wechselkurs für Schweden beträgt 1 Euro > 10.10 Schwedische Kronen!");
					erf = true;
					des = "Schweden";
				}
				else {
					if (des.equalsIgnoreCase("England")) {
						System.out.println("Der Wechselkurs für England beträgt 1 Euro > 0.89 Pfund!");
						erf = true;
						des = "England";
					}
					else {
						if (des.equalsIgnoreCase("Schweiz")) {
							System.out.println("Der Wechselkurs für die Schweiz beträgt 1 Euro > 1.08 Schweizer Franken!");
							erf = true;
							des = "Schweiz";
						}
						else {
							if (des.equalsIgnoreCase("Japan")) {
								System.out.println("Der Wechselkurs für Japan beträgt 1 Euro > 126.50 Yen!");
								erf = true;
								des = "Japan";
							}
							else {
								System.out.println("Bitte wähle eines der folgenden Länder als Ziel: Japan, Schweiz, Schweden, England, USA");
								erf = false;
							}
						}
					}
				}
			}
		}
		
		return des;
	}
	
	
	
	
	
	
	/*public static String dest() {
		Scanner sc = new Scanner(System.in);
		boolean erf = false;
		String des = new String();
		
		while (erf == false) {
			System.out.println("Bitte gib dein nächstes Reiseziel ein:");
			des = sc.next();
			
			if (des.equalsIgnoreCase("USA")) {
				System.out.println("Der Wechselkurs für die USA beträgt 1 Euro > 1.22 Dollar!");
				erf = true;
				des = "USA";
				
			}
			
			if (des.equalsIgnoreCase("Schweden")) {
				System.out.println("Der Wechselkurs für Schweden beträgt 1 Euro > 10.10 Schwedische Kronen!");
				erf = true;
				des = "Schweden";
			}
			
			if (des.equalsIgnoreCase("Schweiz")) {
				System.out.println("Der Wechselkurs für die Schweiz beträgt 1 Euro > 1.08 Schweizer Franken!");
				erf = true;
				des = "Schweiz";
			}
			
			if (des.equalsIgnoreCase("Japan")) {
				System.out.println("Der Wechselkurs für Japan beträgt 1 Euro > 126.50 Yen!");
				erf = true;
				des = "Japan";
			}
			
			if (des.equalsIgnoreCase("England")) {
				System.out.println("Der Wechselkurs für England beträgt 1 Euro > 0.89 Pfund!");
				erf =  true;
				des = "England";
			}
			
			else {
				System.out.println("Bitte wähle eines der folgenden Länder: USA, Schweiz, Schweden, England, Japan!");
				erf = false;
			}
		}
		
		return des;
	}*/

}
