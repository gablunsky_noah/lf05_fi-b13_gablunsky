public class Ladung
{


    private String bezeichnung;
    private int menge;

    public Ladung(String bezeichnung, int menge)
    {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    // Verwaltungsmethoden

    public String getBezeichnung()
    {
        return this.bezeichnung;
    }

    public void setBezeichnung( String y )
    {
        this.bezeichnung = y;
    }

    public int getMenge()
    {
        return this.menge;
    }

    public void setMenge( int y )
    {
        this.menge = y;
    }
}