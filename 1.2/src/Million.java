//Entwickeln Sie ein Programm, welches die Anzahl der Jahre berechnet, bis Sie aufgrund
//einer einmaligen Einlage und einem konstanten Zinssatz Million�r geworden sind. Die H�he
//der Einlage und der Zinssatz sollen vom Benutzer eingegeben werden. Die Anzahl der Jahre
//(auf ganze Jahre aufgerundet) soll vom Programm ausgegeben werden.
//Nach der Ausgabe soll der Benutzer gefragt werden, ob er einen weiteren
//Programmdurchlauf mit anderen Werten durchf�hren m�chte. Best�tigt er mit �j�, wird eine
//weitere Rechnung durchgef�hrt, eine Eingabe von �n� beendet das Programm.

import java.util.Scanner;

public class Million {

	public static void main(String[] args) {
		boolean nochmal = false;
		do {
			Scanner sc = new Scanner(System.in);
			String n;
			int ergebnis;
			double einlage;
			double zins;
			System.out.println("Bitte geben Sie die einmalige Einlage ein:");
			einlage = sc.nextDouble();
			System.out.println("Bitte geben Sie den Zinssatz an:");
			zins = sc.nextDouble();
			ergebnis = rechner(einlage, zins);
			System.out.println("Es w�rde " + ergebnis + " Jahre dauern, bis Sie bei einer Million w�ren!");
			System.out.println("M�chten Sie noch einen Versuch starten?(J/N)");
			n =sc.next();
			if(n.equalsIgnoreCase("j")) {
				nochmal = true;
			}
			else {
				nochmal = false;
			}
		}while(nochmal == true);
	}
	
	public static int rechner(double x, double y) {
		int jahre = 0;
		while(x < 1000000) {
			x += x*(y/100);
			jahre++;
		}
		return jahre;
	}

}
