package zweinull;
import java.util.Scanner;
public class Matrix {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl zwischen 2 und 9 ein:");
		int a = sc.nextInt();
		printer(a);
		}
	
	
	public static void printer(int d) {
		int z = 1;
		int c = 1;
		int g = 1;
		while(g<=100) {
			int y = quersumme(g);
			if(c>10) {
				System.out.println();
				c = 1;
			}
			if(y==d) {
				System.out.print("*  ");
			}
			else if(g%d==0) {
				System.out.print("*  ");
			}
			else if(g%10==d || g/10==d) {
				System.out.print("*  ");
			}
			else {
				System.out.print(g + "  ");
			}
			g++;
			c++;
		}
	}
	
	
	public static int quersumme(int x) {
		int quer, rest;
		quer = 0;
		while(x>0) {
			rest = x%10;
			quer += rest;
			x = x/10;
		}
		return quer;
	}

}
