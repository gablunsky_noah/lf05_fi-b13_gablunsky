//Jetzt wird Lotto gespielt. In der Klasse �Lotto� gibt es ein ganzzahliges Array, welches 6 Lottozahlen
//von 1 bis 49 aufnehmen kann. Konkret sind das die Zahlen 3, 7, 12, 18, 37 und 42. Tragen Sie diese im
//Quellcode fest ein.
//a) Geben Sie zun�chst schleifenbasiert den Inhalt des Arrays in folgender Form aus:
//[ 3 7 12 18 37 42 ]
//b) Pr�fen Sie nun nacheinander, ob die Zahlen 12 bzw. 13 in der Lottoziehung vorkommen.
//Geben Sie nach der Pr�fung aus:
//Die Zahl 12 ist in der Ziehung enthalten.
//Die Zahl 13 ist nicht in der Ziehung enthalten.

package zweinull;

public class Arrays4 {

	public static void main(String[] args) {
		int[] x = {3, 7, 12, 18, 37, 42};
		boolean z = false;
		System.out.print("[ ");
		for(int i = 0; i < 5; i++) {
			System.out.print(x[i] + " ");
		}
		System.out.println("]");
		for(int y = 0; y < 5; y++) {
			if(12 == x[y]) {
				System.out.println("12 ist in der Ziehung enthalten.");
				z = true;
			}
		}
		if(z == false)
			System.out.println("12 ist nicht in der Ziehung enthalten.");
		
		for(int b = 0; b < 5; b++) {
			if(13 == x[b]) {
				System.out.println("13 ist in der Ziehung enthalten.");
				z = true;
			}
			else {
				z = false;
			}
		}
		if(z == false)
			System.out.println("13 ist nicht in der Ziehung enthalten.");
	}
}
