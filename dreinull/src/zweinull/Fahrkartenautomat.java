//Der Vorteil der neuen Implementierung ist, dass mein durch die Ver�nderung zweier Werte Fahrkarten + Preise
//ersetzen kann oder durch die Ver�nderung nur eines Wertes den Preis einer Fahrkarte �ndern kann. Au�erdem lassen sich weitere
//Fahrscheine leichter hinzuf�gen. 
//Ein Vorteil der �lteren Version hingegen ist, dass sie �bersichtlicher ist als die neue. 





package zweinull;
import java.util.Scanner;

public class Fahrkartenautomat
{	
	public static void main(String[] args) {
	double zuZahlenderBetrag;
	double rueckgabebetrag;
	double anzahl;
	boolean nochmal = false;
		do{
		anzahl = ticketAnzahl();
		zuZahlenderBetrag = fahrkartenbestellungErfassen(anzahl);
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben(anzahl);
		rueckgeldAusgeben(rueckgabebetrag, anzahl);
		System.out.println("M�chten Sie einen weiteren Kauf t�tigen?(j/n)");
		Scanner sc = new Scanner(System.in);
		String aw = sc.next();
		if (aw.equalsIgnoreCase("j")) {
			nochmal = true;
		}
		else if (aw.equalsIgnoreCase("n")) {
			nochmal = false;
			System.out.println("Auf Wiedersehen!");
		}
		else {
			System.out.println(aw + " ist keine g�ltige Antwort.");
		}
		}while(nochmal == true); 
	}

//-----------------------------------------------------------------------------------------
// Code der die Anzahl der Tickets z�hlt und diese dann ausgibt. Wird ben�tigt, da ich in verschiedenen Methoden mit der Ticket Anzahl arbeite.
		
	public static double ticketAnzahl() {
		Scanner tastatur = new Scanner(System.in);
		double anzahl;
	
		System.out.println("Willkommen! Dies ist ein Fahrkartenautomat. Wie viele Fahrkarten m�chten Sie bestellen?");
		anzahl = tastatur.nextDouble();
		System.out.println("Sie haben " + anzahl + " Fahrkarten bestellt!");
	
		return anzahl;
	}
	
//-----------------------------------------------------------------------------------------
// Code der die Anzahl und den Preis von Tickets multipliziert und als Wert ausgibt.
	
	public static double fahrkartenbestellungErfassen(double x) {
		String name[] = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
		double cost[] = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
		Scanner myScanner = new Scanner(System.in);
        boolean stimmt = true;
        double preis = 0.0;
		do {
        System.out.println("Bitte geben Sie die Nummer ihrer Wunschfahrkarte an:");
        for(int l = 0; l<10; l++) {
        	System.out.println(l + ": " + name[l] + " / " + cost[l] + "�");
        }
        int art = myScanner.nextInt();
        if(art<0 || art >9) {
        	System.out.println(art + " ist keine g�ltige Eingabe!");
        	stimmt = false;
        }
        preis = cost[art] * x;
		}while(stimmt==false);
        return preis;
        
        
        
        
	}
	
//-----------------------------------------------------------------------------------------
// Code der sich um das Bezahlen des Tickets k�mmert und berechnet ob und wieviel R�ckgeld dem Kunden zusteht.
	
	public static double fahrkartenBezahlen(double x) {
	Scanner tastatur = new Scanner(System.in);
	double geld;
	
	System.out.println("\nDer Preis f�r die Tickets liegt bei: " + x + " EURO!"); 
	System.out.println("Bitte bezahlen Sie den Betrag in 5 Cent bis 2 EURO-M�nzen");
    while(x > 0.0)
    {
    	geld = tastatur.nextDouble();
    	x = x - geld;
    	if(x > 0.0)
    	{
    		System.out.println("\nSie haben " + geld + " EURO eingeworfen!\n");
    		System.out.println("Es fehlen noch " + x + " EURO!");
    	}
    }
    if(x < 0.0)
    {
    	x = (x * -1);
    	System.out.println("Ihnen werden " + x + " EURO ausgegeben");
    }
    
    return x;
	}
	
//-----------------------------------------------------------------------------------------
// Code der den Kunden informiert, dass das Ticket, bzw. die Tickets, gedruckt werden.
	
	public static void fahrkartenAusgeben(double x) {
	
	if (x <= 1)
	{
		System.out.println("\nFahrschein wird gedruckt");
	}
	else 
	{
		System.out.printf("\n"+ "%.0f" + " Fahrscheine werden gedruckt\n\n" , x);   
	}
	for (int i = 0; i < 8; i++)
	{
		System.out.print("=");
	    try {
	    	Thread.sleep(250);
			} 
	    catch (InterruptedException e) {
			e.printStackTrace();
			}
	}
	}
	
//-----------------------------------------------------------------------------------------
// Code der das R�ckkeld ausgibt und den Kauf abschlie�t
	
	public static void rueckgeldAusgeben(double x, double y) {
	
    if(x > 0.0)
    {
 	   System.out.printf("\n\nDer R�ckgabebetrag in H�he von" + " %.2f EURO ", (x));
 	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

        while(x >= 2.0) // 2 EURO-M�nzen
        {
     	  System.out.println("2 EURO");
	          x -= 2.0;
        }
        while(x >= 1.0) // 1 EURO-M�nzen
        {
     	  System.out.println("1 EURO");
	          x -= 1.0;
        }
        while(x >= 0.5) // 50 CENT-M�nzen
        {
     	  System.out.println("50 CENT");
	          x -= 0.5;
        }
        while(x >= 0.2) // 20 CENT-M�nzen
        {
     	  System.out.println("20 CENT");
	          x -= 0.2;
        }
        while(x >= 0.1) // 10 CENT-M�nzen
        {
     	  System.out.println("10 CENT");
	          x -= 0.1;
        }
        while(x >= 0.05)// 5 CENT-M�nzen
        {
     	  System.out.println("5 CENT");
	          x -= 0.05;
        }
    }
    if (y <= 1)
    {
 	   System.out.println("\nVergessen Sie nicht, den Fahrschein");
    }
    else 
    {
 	   System.out.println("\nVergessen Sie nicht, die Fahrscheine");
    }
    System.out.println("vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir w�nschen Ihnen eine gute Fahrt.");
 }
}