package zweinull;

public class ArrayHelper {

	public static void main(String[] args) {
		int y[] = {1, 2, 3, 5, 4, 6, 7, 4, 5, 2, 4};
		System.out.print(convertArrayToString(y));
	}
	
	public static String convertArrayToString(int[] zahlen) {
		String x = "";
		for(int i = 0; i< zahlen.length; i++) {
			if(i == zahlen.length-1) {
				x += zahlen[i];
			}
			else {
				x += zahlen[i] + ",";
			}
		}
		return x;
	}

}
